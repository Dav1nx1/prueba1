Prueba.


Actividad a realizar:
Se requiere que usted dise�e un aplicativo que sea capaz de leer una cadena de texto y generar un c�digo QR con dicha informaci�n.
Debe crear un repositorio en bitbucket, y debe colocar el c�digo fuente all�, compartido con el usuario eidast.
Cualquier duda con el requerimiento por favor contactar a Alexander Moreno a trav�s de skype.
Premisas:
-       No existe un lenguaje de programaci�n para realizar esta tarea, elija el que sienta que le va a permitir se m�s productivo en pro de esta prueba.
-       El aplicativo puede ser una aplicaci�n de consola, escritorio o web, sin embargo la misma debe estar disponible para poder ejecutarla.
-       Piense en lo m�s simple primero.
-       La �nica validaci�n que se requiere es que la cadena de entrada sea un URL bien formado.
-       El uso de material de terceros est� permitido, sin embargo se recomienda que conozca bien el funcionamiento del aplicativo que presenta.
-       El formato de salida puede ser: jpg, svg, png